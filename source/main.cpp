#include "c4d.h"
#include <string.h>
#include "main.h"


Bool PluginStart()
{
	if (!EnregistreVoncMatiereUVW())
		return false;
	return true;
}

void PluginEnd()
{
}

Bool PluginMessage(Int32 id, void* data)
{
	switch (id)
	{
	case C4DPL_INIT_SYS:
		if (!g_resource.Init())
			return false;
		return true;
	}
	return false;
}
