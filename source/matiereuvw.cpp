#include "c4d.h"
#include "c4d_symbols.h"
#include "main.h"
#include "vonc_matiereuvw.h"

#define VONC_MATIEREUVW 1037432
#define VERSION "v 1.2"

class MatiereUVW : public ShaderData
{
public:
	BaseShader* shader;
	TexData* texdata;
	String source;
	Int32 sourceExiste;

public:
	virtual Bool Init		(GeListNode* node);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual	Vector Output		(BaseShader* chn, ChannelData* cd);
	virtual Bool Read(GeListNode* node, HyperFile* hf, Int32 level);

	virtual	INITRENDERRESULT InitRender	(BaseShader* chn, const InitRenderStruct& irs);
	virtual	void FreeRender	(BaseShader* chn);

	virtual	SHADERINFO GetRenderInfo(BaseShader* sh);
	virtual BaseShader*	GetSubsurfaceShader(BaseShader* sh, Float& bestmpl);

	static NodeData* Alloc(void) { return NewObjClear(MatiereUVW); }
};

SHADERINFO MatiereUVW::GetRenderInfo(BaseShader* sh)
{
	return SHADERINFO::BUMP_SUPPORT;
}

BaseShader*	MatiereUVW::GetSubsurfaceShader(BaseShader* sh, Float& bestmpl)
{
	if (shader != nullptr)
		return shader->GetSubsurfaceShader(bestmpl);

	return nullptr;
}

Bool MatiereUVW::Init(GeListNode* node)
{
	shader = nullptr;

	BaseContainer* data = ((BaseShader*)node)->GetDataInstance();

	data->SetLink(VONC_MATIEREUVW_TEXTURE, nullptr);
	data->SetString(VONC_MATIEREUVW_SOURCE, ""_s);

	return true;
}

Bool MatiereUVW::Read(GeListNode* node, HyperFile* hf, Int32 level)
{
	if (hf->GetFileVersion() < 8300)
	{
		if (!hf->ReadChannelConvert(node, VONC_MATIEREUVW_TEXTURE))
			return false;		// convert old basechannel
	}

	return true;
}

Vector MatiereUVW::Output(BaseShader* chn, ChannelData* cd)
{
	if (!shader)
		return Vector(1.0);

	Vector uv = cd->p;

	if (cd->vd) {

		if (!texdata && sourceExiste) {
			const RayObject *ro = cd->vd->op;
			if (ro) {
				for (Int32 i = 0, imax = ro->texcnt; i < imax; i++) {
					TexData* td = cd->vd->GetTexData(ro, i);
					if (td) {
						BaseTag* texSource = td->link;
						if (texSource && texSource->GetName() == source) {
							texdata = td;
							break;
						}
					}
				}
				sourceExiste = 0;
			}
		}

		if (texdata) {
			Vector uv2;
			if (cd->vd->ProjectPoint(texdata, cd->vd->lhit, cd->vd->p, cd->vd->n, &uv2)) {
				cd->p.x = uv2.x;
				cd->p.y = uv2.y;
			}
			else {
				//cd->p.x = 0.0;
				//cd->p.y = 0.0;
				return Vector();
			}
		}
	}

	Vector res = shader->Sample(cd);
	cd->p = uv;

	return res;
}

INITRENDERRESULT MatiereUVW::InitRender(BaseShader* chn, const InitRenderStruct& irs)
{
	BaseContainer* data = chn->GetDataInstance();

	data->SetString(VONC_MATIEREUVW_SIGNE, String(VERSION) + " - code.vonc.fr");
	shader = (BaseShader*)data->GetLink(VONC_MATIEREUVW_TEXTURE, irs.doc, Xbase);
	source = data->GetString(VONC_MATIEREUVW_SOURCE);
	sourceExiste = source.GetLength();
	texdata = nullptr;

	if (shader)
		return shader->InitRender(irs);

	return INITRENDERRESULT::OK;
}

void MatiereUVW::FreeRender(BaseShader* chn)
{
	if (shader)
		shader->FreeRender();
	shader = nullptr;
	texdata = nullptr;
}

Bool MatiereUVW::Message(GeListNode* node, Int32 type, void* msgdat)
{
	BaseContainer* data = ((BaseShader*)node)->GetDataInstance();

	HandleInitialChannel(node, VONC_MATIEREUVW_TEXTURE, type, msgdat);
	HandleShaderMessage(node, (BaseShader*)data->GetLink(VONC_MATIEREUVW_TEXTURE, node->GetDocument(), Xbase), type, msgdat);

	return true;
}

Bool EnregistreVoncMatiereUVW(void)
{
	return RegisterShaderPlugin(VONC_MATIEREUVW, GeLoadString(VONC_MATIEREUVW_NOM), 0, MatiereUVW::Alloc, "vonc_matiereuvw"_s, 0);
}

