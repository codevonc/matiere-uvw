#ifndef vonc_matiereuvw_H__
#define vonc_matiereuvw_H__

enum
{
	VONC_MATIEREUVW_SOURCE				= 1000,
	VONC_MATIEREUVW_TEXTURE				= 1001,
	VONC_MATIEREUVW_SIGNE				= 1002
};

#endif // vonc_matiereuvw_H__
